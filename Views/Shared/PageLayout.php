<?php
  ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean();
?>

<header class="page-header">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-11 py-5 d-flex">
        <h1>
          <span class="page-title">
            <?php
              if (isset($page_title)){
                echo $page_title;
              }
              
              else echo $page_content;
            ?>
          </span>
        </h1>
      </div>
    </div>
  </div>
</header>

<section class="container">
  <div class="row">
    <div class="col-11">
      <?php echo $content ?>
    </div>
  </div>
</section>
