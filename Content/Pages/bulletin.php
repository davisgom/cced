<section class="intro">
    <div class="row">
        <div class="col">
            <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Officiis excepturi eligendi mollitia explicabo quia nesciunt ratione sunt perspiciatis voluptatibus eius ducimus unde porro harum, blanditiis perferendis a repellat voluptatum ipsa.
            </p>
        
            <a class="btn btn-theme btn-theme-secondary" href="#">Signup &nbsp; <i class="far fa-envelope"></i></a>
        </div>
    </div>
</section>

<hr class="my-5" />

<section class="current-bulletin">
    <h2 class="mb-4">Current Issue</h2>

    <div class="bulletin-content">
        
        <div class="row">
            <div class="col mb-4">
                <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit.</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <img src="Content/Images/placeholder-image.jpg" class="img-fluid" alt="image for bulletin" />
            </div>

            <div class="col-md-9">
                <p>
                    <strong class="pl-20">In this issue:</strong>
                </p>

                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                    <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                    <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                    <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                    <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                    <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                    <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                    <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<br />
<br />

<section class="past-issues">
    <div class="row">
        <div class="col">
            <h2>Past Issues</h2>

            <div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                    <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                    <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                    <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                    <li class="list-group-item"><a href="#">Lorem ipsum dolor sit amet consectetur, adipisicing elit.</a></li>
                    <li class="list-group-item"><a href="#">Tempore, quo eos. Tenetur adipisci quidem ut sint porro officia.</a></li>
                    <li class="list-group-item"><a href="#">Fugiat deserunt dolore sed modi. Veniam, ipsum dolores?</a></li>
                    <li class="list-group-item"><a href="#">Molestiae a quas veniam?</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<hr class="my-5" />

<section class="small">
    <div class="row">
        <div class="col">
            <p>
                Please contact our office at 517-353-9555 or <a href="#">ced@msu.edu</a> if you would like a copy of one of our reports/publications.
            </p>
        </div>
    </div>
</section>